    import S from 's-js'
    import SArray from 's-array'
    import data from 'surplus-mixin-data'
    Surplus =require 'surplus'
    cx = require 'classnames'

    view = ->
      time = S.data null
      setInterval (-> time new Date), 1000

      <div class="window">
        <header class="toolbar toolbar-header">
          <h1 class="title">💖 Hello World!</h1>
        </header>
        <div class="window-content">
          <p>Welcome to your Electron application.</p>
          <p>The time is <span>{time()?.toLocaleTimeString()}</span>.</p>
        </div>
        <footer class="toolbar toolbar-footer">
          <h1 class="title">Footer</h1>
        </footer>
      </div>

    export default view
