module.exports = [
  // Add support for native node modules
  {
    test: /\.node$/,
    use: 'node-loader',
  },
  {
    test: /\.(m?js|node)$/,
    parser: { amd: false },
    use: {
      loader: '@marshallofsound/webpack-asset-relocator-loader',
      options: {
        outputAssetBase: 'native_modules',
      },
    },
  },
  {
    test: /\.coffee\.md$/,
    exclude: /(node_modules|.webpack)/,
    use: [
      {loader: 'surplus-loader'},
      {loader: 'coffee-loader', options: {literate:true}}
    ]
  }
];
