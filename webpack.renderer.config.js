const rules = require('./webpack.rules');

rules.push({
  test: /\.css$/,
  // use: [{ loader: 'style-loader/url' }, { loader: 'file-loader' }],
  use: ['style-loader','css-loader'],
});
rules.push({
  test: /\.(eot|svg|ttf|woff)$/,
  // use: [{ loader: 'file-loader',options:{name:'[name].[ext]',outputPath:'fonts/'} }],
  use: ['file-loader'],
});

module.exports = {
  // Put your normal webpack config below here
  module: {
    rules,
  },
};
